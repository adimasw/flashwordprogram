`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    22:04:07 05/14/2018 
// Design Name: 
// Module Name:    flashwordprogram_top 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module flashwordprogram_top(input system_clk_p,
	input system_clk_n,
    inout [15:0] bpi_data,
    output [25:0] bpi_addr_cmd,
    output bpi_we_n,
    output bpi_ce_n,
    output bpi_oe_n,
    output bpi_adv
    );
clockgen clkgen(.CLK_IN1_P(system_clk_p), .CLK_IN1_N(system_clk_n),.CLK_OUT1(clk));	 
reg[15:0] data=0;
reg we_n=0;
reg ce_n=0;
reg oe_n=1;
reg adv=0;
reg led=0;

reg[25:0] addr=26'h010000;
reg[3:0] state=0;
reg [8:0] count_to_512;
assign bpi_addr_cmd=addr;
assign bpi_we_n=we_n;
assign bpi_oe_n=oe_n;
assign bpi_ce_n=ce_n;
assign bpi_adv=adv;
assign bpi_data=data;

always @(posedge clk)
begin
	case (state)
	4'h0:
	begin
		we_n<=0;
		oe_n<=1;
		ce_n<=0;
		data<=16'h60;
		state<=4'h1;
	end
	4'h1:
	begin
	we_n<=1;
	state<=4'h2;
	end
	4'h2:
	begin
		we_n<=0;
		data<=16'hD0;
		state<=4'h3;
	end
	4'h3:
	begin
		we_n<=1;
		state<=4'h4;
	end
	4'h4:
	begin
		we_n<=0;
		oe_n<=1;
		ce_n<=0;
		data<=16'h40;
		state<=4'h5;
	end
	4'h5:
	begin
		we_n<=1;
		state<=4'h6;
	end
	4'h6:
		begin
		we_n<=0;
		data<=16'h53;
		state<=4'h7;
		end
	4'h7:
	begin
	we_n<=1;
	oe_n<=1;
	ce_n<=1;
	end
endcase
end
endmodule
